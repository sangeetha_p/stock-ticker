from django import forms
from .models import SignUp
from .models import StockInfo
import os, sys

class SignUpForm(forms.ModelForm):
    class Meta:
        model = SignUp
        fields=["first_name","last_name","email"]
        
class StockTickerForm(forms.ModelForm):
    class Meta:
        model = StockInfo
        fields=["stock_ticker"]        
       
class PostForm(forms.Form):
    stock_ticker = forms.CharField(max_length=256)
    #created_at = forms.DateTimeField()