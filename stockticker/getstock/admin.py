from django.contrib import admin

from getstock.models import StockInfo
from getstock.models import SignUp

admin.site.register(StockInfo)
admin.site.register(SignUp)

# Register your models here.
