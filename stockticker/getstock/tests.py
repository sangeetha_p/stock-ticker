from django.test import TestCase
from getstock.models import StockInfo

"""StockInfo model tests."""
def test_str(self):
    stock_info = StockInfo(stock_ticker='BBRY')
    
    self.assertEquals(str(stock_info),'BBRY'
                      )
# Create your tests here.
