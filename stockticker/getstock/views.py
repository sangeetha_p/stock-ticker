from django.views.generic import ListView
from getstock.models import StockInfo

from django.shortcuts import render,get_object_or_404,RequestContext
from django.shortcuts import render_to_response
from django.http import HttpResponse
from getstock.forms import SignUpForm
from getstock.forms import StockTickerForm


import urllib
import re
 
from getstock.forms import PostForm       

# Function that gets the stock price, 
# needs the stock ticker as parameter

def getStockPrice(stock_ticker):
    url = "http://finance.yahoo.com/q?s=" + stock_ticker +"&ql=i"
    htmlFile = urllib.urlopen(url)
    htmlText = htmlFile.read()
    regex = '<span id="yfs_l84_[^.]*">(.+?)</span>'
    pattern = re.compile(regex)
    price = re.findall(pattern,htmlText)
   # print "The price of", stock_ticker, "is", price[0]
    return price

def stocks_in_db(request):
    #latest_stocks_list = 
    context = RequestContext(request)
    stock_list = StockInfo.objects.all()
    stock_dict = {'stocks' : stock_list}
    
 
    return stock_dict

def home(request):
    
    return render_to_response("getstock/signup.html",locals(),context_instance=RequestContext(request))

def stock_ticker_home(request):
    
    form = StockTickerForm(request.POST  or None)
    if form.is_valid():
        save_it = form.save(commit=False)
        save_it.save()
    ticker_value = []
    tic_val = []
    if request.POST:    
       # print "request.POST :", request.POST
        #print "request:", request
        print "request.POST[stock_ticker] :", request.POST["stock_ticker"]
        ticker = request.POST["stock_ticker"]
        #print "Stock Price", getStockPrice(ticker)
        ticker_value = getStockPrice(ticker)
#         print "Stock Price", ticker_value[0]
        if ticker_value == []:
            tic_val = "None"
        else:
            tic_val = str(ticker_value[0])
        #val =  stocks_in_db(request)
        #print val

    return render_to_response("getstock/stock_ticker_home.html",locals(),context_instance=RequestContext(request))
        
        
