from django.db import models
from django.utils import timezone
from django.core.urlresolvers import reverse
from django.contrib import admin
from django.utils.encoding import smart_unicode
# Create your models here.

class SignUp(models.Model):
    first_name = models.CharField(max_length=120,null=True,blank=True)
    last_name = models.CharField(max_length=120, null=True, blank=True)
    email = models.EmailField()
    timestamp = models.DateTimeField(auto_now_add = True, auto_now = False)
    updated = models.DateTimeField(auto_now_add=False, auto_now= True)
    
    def __unicode__(self):
        return smart_unicode(self.email)
    
class StockInfo(models.Model):
   stock_ticker = models.CharField(max_length=120, null=False, blank=True)
   #stock_name = models.CharField(max_length=100)
   #add_date = models.DateTimeField('date published')
   
   def __unicode__(self):
        return smart_unicode(self.stock_ticker)
   


 