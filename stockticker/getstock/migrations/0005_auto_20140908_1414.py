# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('getstock', '0004_auto_20140908_0703'),
    ]

    operations = [
        migrations.AlterField(
            model_name='stockinfo',
            name='stock_ticker',
            field=models.CharField(max_length=120, blank=True),
        ),
    ]
