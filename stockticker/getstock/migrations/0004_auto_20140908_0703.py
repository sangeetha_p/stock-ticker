# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('getstock', '0003_auto_20140908_0534'),
    ]

    operations = [
        migrations.AlterField(
            model_name='stockinfo',
            name='stock_ticker',
            field=models.CharField(max_length=20, null=True, blank=True),
        ),
    ]
