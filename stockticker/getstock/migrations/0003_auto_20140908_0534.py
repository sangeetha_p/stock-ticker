# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('getstock', '0002_signup'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='stockinfo',
            name='add_date',
        ),
        migrations.RemoveField(
            model_name='stockinfo',
            name='stock_name',
        ),
        migrations.AlterField(
            model_name='stockinfo',
            name='stock_ticker',
            field=models.CharField(max_length=120, null=True, blank=True),
        ),
    ]
