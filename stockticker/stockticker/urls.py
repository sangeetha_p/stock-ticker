from django.conf.urls import patterns, include, url
from django.contrib import admin
import getstock.views


urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'getstock.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),
   
    url(r'^$', 'getstock.views.stock_ticker_home',name='stock_ticker_home'),
    url(r'^getstock/', include('getstock.urls')),
    url(r'^post/form_upload.html$',
        'getstock.views.post_form_upload', name='post_form_upload'),
    url(r'^admin/', include(admin.site.urls)),
)
